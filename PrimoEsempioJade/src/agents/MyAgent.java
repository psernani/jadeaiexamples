package agents;

import behav.MyBehav;
import behav.MyCyclic;
import behav.MyOneShot;
import behav.MyTicker;
import behav.MyWaker;
import behav.ReceiveMessage;
import behav.SendMessage;
import jade.core.Agent;

public class MyAgent extends Agent
{
	public void setup()
	{
		// AID: GUID + addresses
		// GUID: Nome Locale @ Nome AP
		// Nome AP: IP:1099/JADE
		// GUID ex: Pippo@127.0.0.1:1099/JADE
		System.out.println(this.getLocalName() + ": Inizializzazione...");
		
		//this.addBehaviour(new MyBehav(this));
		//this.addBehaviour(new MyOneShot(this));
		//this.addBehaviour(new MyCyclic(this, "ciao"));
		//this.addBehaviour(new MyCyclic(this, "arrivederci"));
		//this.addBehaviour(new MyWaker(this, 5000));
		//this.addBehaviour(new MyTicker(this, 2000));
		Object[] args = this.getArguments();
		if (args != null)
		{
			if (args[0].toString().equals("send"))
			{
				this.addBehaviour(new SendMessage(this, "Pluto"));
				this.addBehaviour(new MyTicker(this, 500));
			}
		}
		
		this.addBehaviour(new ReceiveMessage(this));
	}
}
