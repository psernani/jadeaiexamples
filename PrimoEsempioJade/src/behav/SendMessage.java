package behav;

import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.OneShotBehaviour;
import jade.lang.acl.ACLMessage;

public class SendMessage extends OneShotBehaviour
{
	private String name;
	public SendMessage(Agent schedAgent, String name)
	{
		super(schedAgent);
		this.name = name;
	}
	
	public void action()
	{
		AID receiver = new AID();
		receiver.setLocalName(name);
		
		ACLMessage msg = new ACLMessage(ACLMessage.INFORM);
		msg.addReceiver(receiver);
		msg.setContent("Ciao - messaggio da: " + this.myAgent.getLocalName());
		
		this.myAgent.send(msg);
	}

}
