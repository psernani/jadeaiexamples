package behav;

import jade.core.Agent;
import jade.core.behaviours.TickerBehaviour;

public class MyTicker extends TickerBehaviour
{
	private long currentTime;
	
	public MyTicker(Agent schedAgent, long period)
	{
		super(schedAgent, period);
		currentTime = System.currentTimeMillis();
	}
	
	
	protected void onTick()
	{
		long now = System.currentTimeMillis();
		long timePast = now - currentTime;
		currentTime = now;
		
		System.out.println(this.myAgent.getLocalName() + ": My ticker " + timePast);
	}

}
