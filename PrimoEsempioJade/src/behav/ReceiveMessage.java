package behav;

import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.behaviours.WakerBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

public class ReceiveMessage extends CyclicBehaviour
{
	public ReceiveMessage(Agent schedAgent)
	{
		super(schedAgent);
	}
	
	@Override
	public void action()
	{
		//ACLMessage msg = this.myAgent.receive();
		//ACLMessage msg = this.myAgent.blockingReceive();
		ACLMessage msg = this.myAgent.receive(MessageTemplate.MatchPerformative(ACLMessage.REQUEST));
		
		if (msg != null)
		{
			System.out.println(this.myAgent.getLocalName() + ": ho ricevuto un messaggio");
			System.out.println(this.myAgent.getLocalName() + ": il contenuto e' " +
						msg.getContent());
			
			String sender = msg.getSender().getLocalName();
			
			this.myAgent.addBehaviour( new WakerBehaviour(this.myAgent, 5000) {
				protected void onWake()
				{
					this.myAgent.addBehaviour(new SendMessage(this.myAgent, sender));
				}
			});
		}
		else
		{
			this.block();
		}
	}
}
