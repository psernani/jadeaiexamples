package behav;

import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;

public class MyCyclic extends CyclicBehaviour
{
	private String stringa; 
	
	public MyCyclic(Agent schedAgent, String stringa)
	{
		super(schedAgent);
		this.stringa = stringa;
	}
	
	public void action()
	{
		//while (true)
		//{
			System.out.println(this.myAgent.getLocalName() + ": My cyclic behaviour: " + stringa);
		//}
	}
}
