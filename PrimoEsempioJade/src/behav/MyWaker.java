package behav;

import jade.core.Agent;
import jade.core.behaviours.WakerBehaviour;

public class MyWaker extends WakerBehaviour
{
	private long currentTime;
	
	public MyWaker(Agent schedAgent, long timeout)
	{
		super(schedAgent, timeout);
		currentTime = System.currentTimeMillis();
	}
	
	protected void onWake()
	{
		long timePast = System.currentTimeMillis() - currentTime;
		System.out.println(this.myAgent.getLocalName() + ": My waker " + timePast);
	}
}
