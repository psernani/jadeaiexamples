package behav;

import jade.core.Agent;
import jade.core.behaviours.Behaviour;

public class MyBehav extends Behaviour
{
	private int counter;
	
	public MyBehav(Agent schedAgent)
	{
		super(schedAgent);
		counter = 0;
	}

	@Override
	public void action()
	{
		//while (counter < 3)
		//{
			System.out.println(this.myAgent.getLocalName() + ": " + counter);
			counter++;
		//}
	}

	@Override
	public boolean done()
	{
		if (counter < 3)
		{
			return false;
		}
		
		return true;
	}

}
