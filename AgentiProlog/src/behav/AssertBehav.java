package behav;

import org.jpl7.Query;

import jade.core.Agent;

public class AssertBehav extends MyOneShot
{
	private String predName;
	private String argument;
	
	public AssertBehav(Agent schedAgent, String predName, String argument)
	{
		super(schedAgent);
		this.predName = predName;
		this.argument = argument;
	}
	
	public void action()
	{
		// assert(somma(12)).
		String goal = "assert(" + predName + "(" + argument + "))";
		Query q = new Query(goal);
		
		if (q.hasSolution())
		{
			myPrint(goal);
			this.myAgent.addBehaviour(new GoalProof(
					this.myAgent,
					predName + "(X)",
					"X"));
		}
	}
}
