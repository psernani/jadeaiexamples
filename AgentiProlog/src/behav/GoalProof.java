package behav;

import java.util.Map;

import org.jpl7.Query;
import org.jpl7.Term;

import jade.core.Agent;

public class GoalProof extends MyOneShot
{
	private String goal;
	private String variable;
	
	public GoalProof(Agent schedAgent, String goal, String variable)
	{
		super(schedAgent);
		this.goal = goal;
		this.variable = variable;
	}
	
	public void action()
	{
		Query q = new Query(goal);
		
		Map<String, Term>[] solutions = q.allSolutions();
		
		for (Map<String, Term> sol : solutions)
		{
			Term result = sol.get(variable);
			myPrint("Il valore della somma recuperato dall'engine e' " + result.toString());
		}
	}
}
