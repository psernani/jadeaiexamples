package behav;

import java.util.Map;

import org.jpl7.Query;
import org.jpl7.Term;

import jade.core.Agent;

public class QueryProlog extends MyOneShot
{
	private String filename;
	private String goal;
	private String variable;
	
	public QueryProlog(Agent schedAgent, String filename, String goal, String variable)
	{
		super(schedAgent);
		this.filename = filename;
		this.goal = goal;
		this.variable = variable;
	}
	
	public void action()
	{
		String consult = "consult('" + filename + "')";
		Query q = new Query(consult);
		
		if (q.hasSolution())
		{
			myPrint(consult);
			
			q = new Query(goal);
			
			Map<String, Term>[] solutions = q.allSolutions();
			
			for (Map<String, Term> sol : solutions)
			{
				Term result = sol.get(variable);
				myPrint(result.toString());
				Term[] elements = result.toTermArray();
				
				int somma = 0;
				for (Term el : elements)
				{
					somma += Integer.parseInt(el.toString());
				}
				
				myPrint("La somma e': " + somma);
				this.myAgent.addBehaviour(new AssertBehav(
						this.myAgent,
						"somma",
						somma + ""
					));
			}
		}
	}
}
