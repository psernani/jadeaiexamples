package agents;

import behav.QueryProlog;
import jade.core.Agent;

public class IntelligentAgent extends Agent
{
	public void setup()
	{
		System.out.println(this.getLocalName() + ": init...");
		this.addBehaviour(new QueryProlog(
				this,
				"rimuoviDispari.pl",
				"rimuoviDispariEOrdina([5,3,2,4,1,6], Out)",
				"Out"
			));
	}
}
