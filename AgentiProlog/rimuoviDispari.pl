rimuoviDispariEOrdina(L,Out) :- rimuoviDispari(L,LNoDisp),
                             quick(LNoDisp, Out).
                             
rimuoviDispari([],[]).
rimuoviDispari([T|C],[T|Out]) :- X is T mod 2,
                              X = 0,
                              !,
                              rimuoviDispari(C,Out).
rimuoviDispari([_|C],Out) :- rimuoviDispari(C,Out).

quick([],[]).
quick([T|C],L) :- split(C,T,L1,L2),
               quick(L1, LMin),
               quick(L2, LMax),
               append(LMin, [T|LMax], L).
               
split([],_,[],[]).
split([T|C],N,[T|L1], L2) :- T =< N,
                          !,
                          split(C,N,L1,L2).
split([T|C],N,L1,[T|L2]) :- split(C,N,L1,L2).